# README #

### What is this repository for? ###
Deploying your application using Blue-Green Deployment over a Kubernetes Cluster.

### How do I get set up? ###
Download the Gradle plugin.
Add it to your local repository.
Run the kbg task!

### Contribution guidelines ###
Any improvement is appreciated.

### Who do I talk to? ###

kr76@st-andrews.ac.uk