package kr.kubernetes;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

/**
 * This class defines the Gradle task.
 */
public class KBGTask extends DefaultTask {
    /**
     * This method will be called when the task is executed.
     */
    @TaskAction
    public void bg() {
        KBGPluginExtension extension = getProject().getExtensions().findByType(KBGPluginExtension.class);
        if (extension == null) {
            System.exit(0);
        }
        new KubernetesBG (extension);
    }
}
