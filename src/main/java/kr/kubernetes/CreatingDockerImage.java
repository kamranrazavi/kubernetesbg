package kr.kubernetes;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.ProgressHandler;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ProgressMessage;
import com.spotify.docker.client.messages.RegistryAuth;

import java.io.*;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicReference;

/**
 * This class connects to the docker, creates an image from a Dockerfile,
 *  changes the image's tag for pushing to the docker hub repository, and push it to the repository.
 */
public class CreatingDockerImage {

    DockerClient docker;

    /**
     * Constructor.
     * @param path path to the Dockerfile.
     * @param name name of the Docker image.
     * @param email email of the user.
     * @param username username of the user.
     * @param password password of the user.
     */
    public CreatingDockerImage(String path, String name, String email, String username, String password) {
        try {
            // Create a client based on DOCKER_HOST and DOCKER_CERT_PATH env vars
            docker = DefaultDockerClient.fromEnv().build();

        } catch(Exception e) {
            e.printStackTrace();
            System.exit(0);
        }

        //Creating an image from Dockerfile
        createImageFromDockerfile(path, name);

        //Changing name for pushing to a repository
        tagImage(name, username+"/"+name);

        //Pushing an image to a repository
        pushingImage(username+"/"+name, email, username, password);

        //Closing the client
        docker.close();

    }

    /**
     * This method authenticates the user and pushes a Docker image to the Docker hub.
     * @param tagName Docker image tag.
     * @param email email of the user.
     * @param username username of the user.
     * @param password password of the user.
     */
    private void pushingImage(String tagName, String email, String username, String password) {
        final RegistryAuth registryAuth = RegistryAuth.builder()
                .email(email)
                .username(username)
                .password(password)
                .build();
        try {
            docker.push(tagName, registryAuth);
        } catch (DockerException e) {
            e.printStackTrace();
            System.out.println("Couldn't create image.");
            System.exit(0);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Couldn't create image.");
            System.exit(0);
        }

    }

    /**
     * This method changes the tag of a Docker image.
     * @param name current name of the Docker image.
     * @param tagName newer name of the Docker image.
     */
    private void tagImage(String name, String tagName) {
        try {
            docker.tag(name, tagName);
        } catch (DockerException e) {
            e.printStackTrace();
            System.out.println("Couldn't create image.");
            System.exit(0);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Couldn't create image.");
            System.exit(0);
        }

    }

    /**
     * This method creates a Docker image from a Dockerfile.
     * @param path path to the Dockerfile.
     * @param name name of the new Docker image.
     */
    private void createImageFromDockerfile(String path, String name) {
        final AtomicReference<String> imageIdFromMessage = new AtomicReference<>();

        try {
            docker.build(
                    Paths.get(path), name, new ProgressHandler() {
                        @Override
                        public void progress(ProgressMessage message) throws DockerException {
                            final String imageId = message.buildImageId();
                            if (imageId != null) {
                                imageIdFromMessage.set(imageId);
                            }
                        }
                    });

        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Couldn't create image.");
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Couldn't create image.");
            System.exit(0);
        } catch (DockerException e) {
            e.printStackTrace();
            System.out.println("Couldn't create image.");
            System.exit(0);
        }
    }

}
