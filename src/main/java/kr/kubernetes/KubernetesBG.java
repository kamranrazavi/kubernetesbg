package kr.kubernetes;

import io.fabric8.kubernetes.api.Controller;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodList;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Ref;
import org.gradle.tooling.BuildLauncher;
import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ProjectConnection;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * This class does a blue green deployment technique over a Kubernetes cluster.
 */
public class KubernetesBG {

    final private String DEFAULT_PORT = "31235";
    private ArrayList<File> yamlFiles, blueYamlFiles;
    private ArrayList<File> blueServices;
    private ArrayList<String> servicesNames;
    private String port;
    private KubernetesClient client;
    private File nodePortFile;
    private Controller controller;
    private ArrayList<NameType> nameTypes;
    private KBGPluginExtension extension;
    private String tag;
    private File previousNodePortFile;

    /**
     * Constructor.
     * @param e user's input.
     */
    public KubernetesBG (KBGPluginExtension e) {
        this.extension = e;

        //Sets system property
        setSystemProperties();

        //Find latest tag
        findLatestTag();

        extension.setDockerImageName(extension.getDockerImageName() + ":" + tag);

        //Create a new Docker image
        new CreatingDockerImage(extension.getDockerFilePath(), extension.getDockerImageName(),
                extension.getDockerHubEmail(), extension.getDockerHubUserName(), extension.getDockerHubPassword());

        //Create duplicates of all yaml files
        duplicateYamlFiles(extension.getYamlFilesPath(), tag);

        //find services
        findServices();

        String dockerImageName = extension.getDockerHubUserName() + "/" + extension.getDockerImageName();
        //change metadata name and selector app
        changeBlueYamlFiles(dockerImageName, extension.getPreviousVersion(), tag);

        //Kubernetes client
        runBlueFiles();

        //Integration tests
        if (runIntegratedTest()) {
            if (extension.getStrategy().equals("switch")) {
                long startTime = System.nanoTime();
                switchApplication();
                long endTime = System.nanoTime();
                long duration = (endTime - startTime);
                System.out.println("Time: " + duration/1000000);
                System.out.println("Kubernetes blue/green deployment is done.");
                //Delete previous version
                deleteYamlFiles(yamlFiles);
                try {
                    FileUtils.copyFile(nodePortFile, previousNodePortFile);
                    FileUtils.forceDelete(nodePortFile);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            } else if (extension.getStrategy().equals("ports")) {
                System.out.println("The blue environment can be accessed at port " + DEFAULT_PORT);
            } else {
                System.out.println("Task " + extension.getNextTask() + " is now executing.");
                ProjectConnection connection = GradleConnector.newConnector()
                        .forProjectDirectory(new File(extension.getConsumerPath()))
                        .connect();
                try {
                    BuildLauncher build = connection.newBuild();
                    build.forTasks(extension.getNextTask());
                    build.setStandardOutput(System.out);
                    build.run();
                } finally {
                    connection.close();
                }
            }
        } else {
            System.err.println("Tests are not passed!");
            clearFiles();
        }
    }

    /**
     * This method find the latest tag in the local repository.
     */
    private void findLatestTag() {
        String repoPath = extension.getGitPath();
        try {
            Git git = Git.open(new File(repoPath));
            Map<String, Ref> map = git.getRepository().getTags();
            for (String s: map.keySet()) {
                tag = s;
            }
            if (tag == null) {
                System.out.println("No tag was available.");
                System.exit(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("No tag was available.");
            System.exit(0);
        }

    }

    private void clearFiles() {
        System.err.println("Clearing newer version.");
        deleteYamlFiles(blueYamlFiles);
        try {
            FileUtils.forceDelete(nodePortFile);
        } catch (IOException e) {

        }
        System.exit(0);
    }

    /**
     * This method sets System properties for connecting to the Kubernetes cluster and Docker host.
     */
    private void setSystemProperties() {
        System.setProperty("kubernetes.master", extension.getKubernetesMaster());
        System.setProperty("kubernetes.certs.ca.file", extension.getKubernetesCertsCaFile());
        System.setProperty("kubernetes.certs.client.file", extension.getKubernetesCertsClientFile());
        System.setProperty("kubernetes.certs.client.key.file", extension.getKubernetesCertsClientKeyFile());
        System.setProperty("kubernetes.namespace", extension.getKubernetesNamespace());
        System.setProperty("docker.host", extension.getDockerHost());
    }

    /**
     * This method switches previous version services with the new one.
     */
    private void switchApplication() {
        for (String s: servicesNames) {
            client.services().withName(s).delete();
        }
        try {
            controller.apply(nodePortFile);
        } catch (Exception e) {
            e.printStackTrace();
            clearFiles();
        }
        for (NameType nt: nameTypes) {
            stopPreviousOnes(nt);
        }
    }

    /**
     * This method deletes non services types from the cluster.
     * @param nt name and type of the non service to be deleted.
     */
    private void stopPreviousOnes(NameType nt) {
        if (nt.getType().equalsIgnoreCase("deployment")) {
            client.extensions().deployments().withName(nt.getName()).delete();
        } else if (nt.getType().equalsIgnoreCase("ingress")) {
            client.extensions().ingresses().withName(nt.getName()).delete();
        } else if (nt.getType().equalsIgnoreCase("pod")) {
            client.pods().withName(nt.getName()).delete();
        } else if (nt.getType().equalsIgnoreCase("persistentvolume")) {
            client.persistentVolumes().withName(nt.getName()).delete();
        } else if (nt.getType().equalsIgnoreCase("persistentvolumeclaim")) {
            client.persistentVolumeClaims().withName(nt.getName()).delete();
        } else if (nt.getType().equalsIgnoreCase("replicaset")) {
            client.extensions().replicaSets().withName(nt.getName()).delete();
        } else if (nt.getType().equalsIgnoreCase("horizontalpodautoscaler")) {
            client.extensions().horizontalPodAutoscalers().withName(nt.getName()).delete();
        } else if (nt.getType().equalsIgnoreCase("replicationcontroller")) {
            client.replicationControllers().withName(nt.getName()).delete();
        } else if (nt.getType().equalsIgnoreCase("statefulset")) {
            client.apps().statefulSets().withName(nt.getName()).delete();
        }
    }
    private int circle = 0;
    /**
     * Integration tests.
     * @return true, if all pods are running.
     */
    private boolean runIntegratedTest() {
        if (circle == 50) {
            return false;
        }
        circle++;
        int n = client.pods().list().getItems().size();
        try {
            TimeUnit.SECONDS.sleep(n);
        } catch (InterruptedException e) {
            System.out.println("Cannot wait!");
            clearFiles();
        }
        PodList podList = client.pods().list();
        List<Pod> items = podList.getItems();
        for (Pod item : items) {
            if (!item.getStatus().getPhase().equals("Running")) {
                runIntegratedTest();
            }
        }
        return true;
    }

    /**
     * This method runs the newer version.
     */
    private void runBlueFiles() {
        client = new DefaultKubernetesClient();
        try {
            controller = new Controller(client);
            for (int i = 0; i < blueYamlFiles.size() - 1; i++) {
                controller.apply(blueYamlFiles.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
            clearFiles();
        }
    }

    /**
     * This method changes previous yaml files to the newer version.
     * @param dockerImageName new Docker image that should be inserted.
     * @param previousVersion previous version.
     * @param currentVersion newer version.
     */
    private void changeBlueYamlFiles(String dockerImageName, String previousVersion, String currentVersion) {
        for (int i = 0; i < blueYamlFiles.size(); i++) {
            try {
                String path = blueYamlFiles.get(i).getPath();
                boolean srv = false;
                if (blueServices.contains(blueYamlFiles.get(i))) {
                    srv = true;
                }
                BufferedReader file = new BufferedReader(new FileReader(path));
                String line, type = "", name = "";
                StringBuffer inputBuffer = new StringBuffer();
                boolean metadata = false;
                while ((line = file.readLine()) != null) {
                    if (metadata) {
                        if (line.contains("name:")) {
                            if (i != blueYamlFiles.size() - 1) {
                                if (srv) {
                                    servicesNames.add(line.substring(line.indexOf(':') + 2));
                                } else {
                                    name = line.substring(line.indexOf(':') + 2);
                                }
                            }
                            if (line.contains(previousVersion)) {
                                line = line.substring(0, line.indexOf(previousVersion)) + currentVersion;
                            }
                            else {
                                line += "-" + currentVersion;
                            }
                            metadata = false;
                        }
                    }
                    if (line.startsWith("kind: ")) {
                        type = line.substring(line.indexOf(':') + 2);
                    }
                    if (line.contains(dockerImageName.substring(0, dockerImageName.indexOf(':')))) {
                        line = line.substring(0, line.indexOf(dockerImageName.substring(0, dockerImageName.indexOf(':')))) + dockerImageName;
                    }
                    if (line.contains(port) && i != blueYamlFiles.size() - 1) {
                        line = line.substring(0, line.indexOf(port)) + DEFAULT_PORT;
                        previousNodePortFile = new File(path);
                    }
                    if (line.contains("spec:")) {
                        metadata = false;
                    }
                    if (line.contains("metadata:")) {
                        metadata = true;
                        }
                    if (line.contains("app:")) {
                        if (line.contains(previousVersion)) {
                            line = line.substring(0, line.indexOf(previousVersion)) + currentVersion;
                        }
                        else {
                            line += "-" + currentVersion;
                        }
                    }
                    inputBuffer.append(line);
                    inputBuffer.append('\n');
                }
                String inputStr = inputBuffer.toString();
                file.close();
                FileOutputStream fileOut = new FileOutputStream(path);
                fileOut.write(inputStr.getBytes());
                fileOut.close();
                if (!type.equalsIgnoreCase("service")) {
                    nameTypes.add(new NameType(name, type));
                }
            } catch (Exception e) {
                e.printStackTrace();
                clearFiles();
            }

        }
        nodePortFile = blueYamlFiles.get(blueYamlFiles.size() - 1);
    }

    /**
     * This method finds services.
     */
    private void findServices() {
        nameTypes = new ArrayList<>();
        blueServices = new ArrayList<>();
        servicesNames = new ArrayList<>();

        for (int i = 0; i < yamlFiles.size(); i++) {
            File file = yamlFiles.get(i);
            String type = "";
            try {
                Scanner scan = new Scanner(file);
                while (scan.hasNext()) {
                    String line = scan.nextLine();
                    if (line.startsWith("kind: ")) {
                        type = line.substring(line.indexOf(':') + 2);
                    }
                    if (line.contains("nodePort")){
                        port = line.substring(line.indexOf(':') + 2);
                        FileUtils.copyFile(file, new File(file.getPath().substring(0, file.getPath().length() - 5) + "-NodePort" +".yaml"));
                        nodePortFile = new File(file.getPath().substring(0, file.getPath().length() - 5) + "-NodePort" +".yaml");
                    }
                }
                if (type.equalsIgnoreCase("service")) {
                    blueServices.add(blueYamlFiles.get(i));
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                clearFiles();
            } catch (IOException e) {
                e.printStackTrace();
                clearFiles();
            }
        }
        blueYamlFiles.add(nodePortFile);
    }

    /**
     * This method removes previous yaml files.
     * @param files files to be deleted
     */
    private void deleteYamlFiles(ArrayList<File> files) {
        int n = files.size();
        for (int i = 0; i < n; i++) {
            try {
                FileUtils.forceDelete(files.get(i));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method duplicates previous yaml files and renames them for newer version.
     * @param path path to the location of yaml files.
     * @param version newer version of the application.
     */
    private void duplicateYamlFiles(String path, String version) {
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles == null || listOfFiles.length == 0) {
            System.out.println("Folder is empty");
            System.exit(0);
        }
        yamlFiles = new ArrayList<>();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile() && listOfFiles[i].getName().endsWith(".yaml")) {
                yamlFiles.add(listOfFiles[i]);
            }
        }

        blueYamlFiles = new ArrayList<>();
        for (int i = 0; i < yamlFiles.size(); i++) {
            String newPath = yamlFiles.get(i).getPath().substring(0, yamlFiles.get(i).getPath().length() - 5) + "-" + version + ".yaml";
            try {
                FileUtils.copyFile(yamlFiles.get(i), new File(newPath));
                blueYamlFiles.add(new File(newPath));
            } catch (IOException e) {
                e.printStackTrace();
                clearFiles();
            }
        }
    }

}

/**
 * This class objects store names and types of Kubernetes non services for deletion.
 */
class NameType {
    private String name;
    private String type;

    /**
     * Getter for name.
     * @return name.
     */
    public String getName() {
        return name;
    }

    /**
     * Getter for type.
     * @return type.
     */
    public String getType() {
        return type;
    }

    /**
     * Constructor.
     * @param n name of the non service.
     * @param t type of the non service.
     */
    public NameType (String n, String t) {
        name = n;
        type = t;
    }
}
