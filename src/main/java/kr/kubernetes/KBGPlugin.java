package kr.kubernetes;

import org.gradle.api.Plugin;
import org.gradle.api.Project;

/**
 * This class creates a Gradle plugin from KBGTask class.
 */
public class KBGPlugin implements Plugin<Project> {
    /**
     * This method will be called when the gradle build runs.
     * @param project Gradle project.
     */
    @Override
    public void apply(Project project) {
        project.getExtensions().create("KBGSetting", KBGPluginExtension.class);
        project.getTasks().create("kbg", KBGTask.class);
    }
}
