package kr.kubernetes;

/**
 * Setting of the plugin
 * All the methods are getter and setter for the variables.
 */
public class KBGPluginExtension {
    private String dockerFilePath;
    private String dockerImageName;
    private String dockerHubEmail;
    private String dockerHubUserName;
    private String dockerHubPassword;
    private String yamlFilesPath;
    private String previousVersion;
    private String kubernetesMaster;
    private String kubernetesCertsCaFile;
    private String kubernetesCertsClientFile;
    private String kubernetesCertsClientKeyFile;
    private String kubernetesNamespace;
    private String dockerHost;
    private String gitPath;
    private String strategy;
    private String nextTask;
    private String consumerPath;

    public String getConsumerPath() {
        return consumerPath;
    }

    public void setConsumerPath(String consumerPath) {
        this.consumerPath = consumerPath;
    }

    public String getNextTask() {
        return nextTask;
    }

    public void setNextTask(String nextTask) {
        this.nextTask = nextTask;
    }

    public String getStrategy() {
        return strategy;
    }

    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }

    public String getDockerRepository() {
        return dockerRepository;
    }

    public void setDockerRepository(String dockerRepository) {
        this.dockerRepository = dockerRepository;
    }

    private String dockerRepository;

    public String getGitPath() {
        return gitPath;
    }

    public void setGitPath(String gitPath) {
        this.gitPath = gitPath;
    }

    public String getKubernetesMaster() {
        return kubernetesMaster;
    }

    public void setKubernetesMaster(String kubernetesMaster) {
        this.kubernetesMaster = kubernetesMaster;
    }

    public String getKubernetesCertsCaFile() {
        return kubernetesCertsCaFile;
    }

    public void setKubernetesCertsCaFile(String kubernetesCertsCaFile) {
        this.kubernetesCertsCaFile = kubernetesCertsCaFile;
    }

    public String getKubernetesCertsClientFile() {
        return kubernetesCertsClientFile;
    }

    public void setKubernetesCertsClientFile(String kubernetesCertsClientFile) {
        this.kubernetesCertsClientFile = kubernetesCertsClientFile;
    }

    public String getKubernetesCertsClientKeyFile() {
        return kubernetesCertsClientKeyFile;
    }

    public void setKubernetesCertsClientKeyFile(String kubernetesCertsClientKeyFile) {
        this.kubernetesCertsClientKeyFile = kubernetesCertsClientKeyFile;
    }

    public String getKubernetesNamespace() {
        return kubernetesNamespace;
    }

    public void setKubernetesNamespace(String kubernetesNamespace) {
        this.kubernetesNamespace = kubernetesNamespace;
    }

    public String getDockerHost() {
        return dockerHost;
    }

    public void setDockerHost(String dockerHost) {
        this.dockerHost = dockerHost;
    }

    public String getDockerFilePath() {
        return dockerFilePath;
    }

    public void setDockerFilePath(String dockerFilePath) {
        this.dockerFilePath = dockerFilePath;
    }

    public String getDockerImageName() {
        return dockerImageName;
    }

    public void setDockerImageName(String dockerImageName) {
        this.dockerImageName = dockerImageName;
    }

    public String getDockerHubEmail() {
        return dockerHubEmail;
    }

    public void setDockerHubEmail(String dockerHubEmail) {
        this.dockerHubEmail = dockerHubEmail;
    }

    public String getDockerHubUserName() {
        return dockerHubUserName;
    }

    public void setDockerHubUserName(String dockerHubUserName) {
        this.dockerHubUserName = dockerHubUserName;
    }

    public String getDockerHubPassword() {
        return dockerHubPassword;
    }

    public void setDockerHubPassword(String dockerHubPassword) {
        this.dockerHubPassword = dockerHubPassword;
    }

    public String getYamlFilesPath() {
        return yamlFilesPath;
    }

    public void setYamlFilesPath(String yamlFilesPath) {
        this.yamlFilesPath = yamlFilesPath;
    }

    public String getPreviousVersion() {
        return previousVersion;
    }

    public void setPreviousVersion(String previousVersion) {
        this.previousVersion = previousVersion;
    }
}
