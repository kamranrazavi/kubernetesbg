package kr.kubernetes

import org.junit.Test
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.api.Project
import static org.junit.Assert.*

class KBGPluginTest {
    @Test
    public void kbg_plugin_should_add_task_to_project() {
        Project project = ProjectBuilder.builder().build()
        project.getPlugins().apply 'kr.kubernetes.kbg.plugin'
        assertTrue(project.tasks.kbg instanceof KBGTask)
    }
}
